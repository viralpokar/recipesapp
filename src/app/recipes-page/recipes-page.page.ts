import { Component, OnInit } from '@angular/core';
import { Recipe } from './recipes.modal';
import { RecipesService } from './recipes.service';

@Component({
  selector: 'app-recipes-page',
  templateUrl: './recipes-page.page.html',
  styleUrls: ['./recipes-page.page.scss'],
})
export class RecipesPagePage implements OnInit {

  recipes : Recipe[];
  constructor(private recipesService: RecipesService) { }

  ngOnInit() {
    this.recipes = this.recipesService.getAllRecipes();

  }

}
